import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;

public class FunctionGraph{
	public static void main (String args []){
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Program to find Function Graph from cartesian field");
		System.out.println("f(x)=ax^2+bx+c");
		
		try{
			System.out.println("Value of a : ");
			String valA = null;
			try{
				valA=reader.readLine();
			}catch(IOException error){
				System.out.println("Input error"+error.getMessage());
			}
			String path = "valueOfA.txt";
			File valueA = new File(path);
				if(!valueA.exists()){
					valueA.createNewFile();
				}
			FileWriter fileA = new FileWriter(valueA.getAbsoluteFile());
			BufferedWriter bufferedWriter = new BufferedWriter(fileA);
			bufferedWriter.write("value of a : "+valA);
			bufferedWriter.close();
		
		//
		
			System.out.println("Value of b : ");
			String valB = null;
			try{
				valB=reader.readLine();
			}catch(IOException error){
				System.out.println("Input error"+error.getMessage());
			}
			String pathB = "valueOfB.txt";
			File valueB = new File(pathB);
				if(!valueB.exists()){
					valueB.createNewFile();
				}
			FileWriter fileB= new FileWriter(valueB.getAbsoluteFile());
			BufferedWriter bufferedWriterB = new BufferedWriter(fileB);
			bufferedWriterB.write("value of b : "+valB);
			bufferedWriterB.close();
		
		//
		
			System.out.println("Value of c: ");
			String valC = null;
			try{
				valC=reader.readLine();
			}catch(IOException error){
				System.out.println("Input error"+error.getMessage());
			}
			String pathC = "valueOfC.txt";
			File valueC = new File(pathC);
			if(!valueC.exists()){
				valueC.createNewFile();
			}
			FileWriter fileC = new FileWriter(valueC.getAbsoluteFile());
			BufferedWriter bufferedWriterC = new BufferedWriter(fileC);
			bufferedWriterC.write("value of c : "+valC);
			bufferedWriterC.close();
		
			try{
				int  ValOfA = Integer.parseInt(valA);
				int  ValOfB = Integer.parseInt(valB);
				int  ValOfC = Integer.parseInt(valC);
			
			System.out.println();
			System.out.println("function : ");
			System.out.println("f(x) = "+ValOfA+" x^2 + "+ValOfB+" x + "+ValOfC);
			String chart= "f(x) = "+ValOfA+" x^2 + "+ValOfB+" x + "+ValOfC;
			if(ValOfA>0){
				System.out.println("Graph legs up, Minimum Turning Point");
			}else{
				System.out.println("Graph legs down, Maximum Turning Point");
			}
		
			if(ValOfA*ValOfB>0){
				System.out.println("Turning Point is on the right of the y-axis");
			}else if(ValOfA*ValOfB<0){
				System.out.println("Turning Point is on the left of the y-axis");
			}else{
				System.out.println("Turning Point is on the y-axis");
			}
		
			if(ValOfC>0){
				System.out.println("The graph intersects the y-axis above the x-axis");
			}else if(ValOfC<0){
				System.out.println("The graph intersects the y-axis below the x-axis");
			}else{
				System.out.println("The graph through point (0,0)");
			}
		
			int PointOfX = -(ValOfB/(2*ValOfA));
			double count = ValOfA*ValOfC*4;
			double ValueOfD = Math.pow(ValOfB,2)+count;
			double PointOfY = -(ValueOfD/(4*ValOfA));
			System.out.println("Turning point ("+PointOfX+" , "+PointOfY+" ) ");
			}catch(Exception e){
			e.printStackTrace();
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}