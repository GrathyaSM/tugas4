import java.io.IOException;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class PascalTriangle{
	public static void main(String [] args) throws IOException{
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		int columnX;
		int lineY;
		int height;
		String Height;
		
		try(FileWriter file = new FileWriter("ResultOfPascalTriangle.txt")){
			String separate = System.getProperty("line.separator");
			
			Height = bufferedReader.readLine();
			
			height = Integer.parseInt(Height);
			int triangle [][] = new int [100][100];
			for(columnX =1;columnX<=height; columnX++){
				for(lineY=height; lineY>columnX; lineY--){
					file.write("   ");
				}
				
				for(lineY=1; lineY<=columnX; lineY++){
					if(columnX==0 || lineY==columnX){
						triangle[columnX][lineY]=1;
						file.write("  "+triangle[columnX][lineY]+"   ");
					}else{
						triangle[columnX][lineY]=triangle[columnX-1][lineY-1] + triangle[columnX-1][lineY];
						if(triangle[columnX][lineY]>99){
							file.write("  "+triangle[columnX][lineY]+"   ");
						}else if(triangle[columnX][lineY]>9){
							file.write("  "+triangle[columnX][lineY]+"   ");
						}else{
							file.write("  "+triangle[columnX][lineY]+"   ");
						}
					}
				}
				file.write(separate);
			}
			
		}
	}
}