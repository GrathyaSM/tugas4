import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CopyPaste{
	public static void main (String [] args){
		System.out.println("Program for Copy and Paste file");
		InputStream inputStream = null;
		OutputStream outputStream = null;
		BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(System.in));
		try {
			try{
				System.out.print("Input Your Initial Directory (example : C:/InitialFolder/FileName.txt) : "+"\n");
				String directory1 = bufferedReader.readLine();
				File fileDirectory1 = new File(directory1);
				try{
					System.out.print("Input Your Destination Directory (example : C:/DestinationFolder/FileName.txt) : "+"\n");
					String directory2 = bufferedReader.readLine();
					File fileDirectory2 = new File(directory2);
				
					inputStream = new FileInputStream(fileDirectory1);
					outputStream = new FileOutputStream(fileDirectory2);
				
					byte [] value = new byte [2048];
				
					int footage;
					while ((footage = inputStream.read(value))>0){
						outputStream.write(value, 0, footage);
					}
				
				
					inputStream.close();
					outputStream.close();
				
					System.out.println("Your file was successfully copied");
				}
				catch (Exception e){
					System.out.print("Your file is not found or no files");
				}
			}
			catch (Exception e){
				System.out.print("Your file is not found or no files");
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}