import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;

public class PermutationsAndCombinations{
	public static void menu (){
		System.out.println("\nMenu");
		System.out.println("1. Permutations");
		System.out.println("2. Combinations");
		System.out.println("0. Exit");
	}
	
	public static void main (String[]args){
		System.out.println("Program to find the value of combination and permutation");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String input = null;
		int choice = 0;
		do{
			menu();
			System.out.print("\nSubmit your choice : ");
			try{
				input=bufferedReader.readLine();
				try{
					choice=Integer.parseInt(input);
					switch (choice){
						case 1:
							permClass.Permutations();
						break;
						
						case 2:
							combClass.Combinations();
						break;

						case 0:
							System.out.println("Thank you");
						break;
						
						default :
						System.out.println("Your input is incorrect, please re-input your choice");
						break;
					}
				}
				catch(NumberFormatException e){
					System.out.println("Your input is incorrect, please re-input your choice");
				}
			}catch(IOException error){
				System.out.println("Error input"+error.getMessage());
			}
		}while(choice>0);
	}
}

class permClass extends PermutationsAndCombinations{
	public static void Permutations(){
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		try{
				System.out.println("Value Of N : ");
				String content = null;
				try{
					content=bufferedReader.readLine();
				}
				catch(IOException error){
					System.out.println("Input Error"+error.getMessage());
				}
				String path = "ValueOfN.txt";
				File file = new File(path);
				if(!file.exists()){
					file.createNewFile();
				}
				FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
				bufferedWriter.write("Value of N : "+content);
				bufferedWriter.close();
			
			
				System.out.println("Value Of R : ");
				String contentR = null;
				try{
					contentR=bufferedReader.readLine();
				}
				catch(IOException error){
					System.out.println("Input Error"+error.getMessage());
				}
				
				String pathR = "ValueOfR.txt";
				File fileR = new File(pathR);
				if(!fileR.exists()){
					fileR.createNewFile();
				}
				
				FileWriter fileWriterR = new FileWriter(fileR.getAbsoluteFile());
				BufferedWriter bufferedWriterR = new BufferedWriter(fileWriterR);
				bufferedWriterR.write("Value of R : "+contentR);
				bufferedWriterR.close();
		
			String lineN = content;
			String lineR = contentR;
			
				int LineN = Integer.parseInt(lineN);
				int LineR = Integer.parseInt(lineR);
				
				if(LineN>0 && LineR>0 && LineN>LineR){
					int Number = LineN - LineR;
					int value1=1, value2=1, value3 =1;
					
					for(int indexI=1; indexI<=LineN; indexI++){
						value1 = value1*indexI;
					}
					for(int indexI=1; indexI<=LineR; indexI++){
						value2 = value2*indexI;
					}
					for(int indexI=1; indexI<=Number; indexI++){
						value3 = value3*indexI;
					}
					
					int valueOfPermutations = value1/value3;
					System.out.println("Result Of Permutations is : "+valueOfPermutations);
					String result = Integer.toString(valueOfPermutations);
					String pathValue = "ResultOfPermutations.txt";
					File ResultPerm = new File(pathValue);
						if(!ResultPerm.exists()){
							ResultPerm.createNewFile();
						}
					FileWriter fileResultC = new FileWriter(ResultPerm.getAbsoluteFile());
					BufferedWriter bufferedResult = new BufferedWriter(fileResultC);
					bufferedResult.write("Result Of Permutations is : "+result);
					bufferedResult.close();
				}
				else{
					System.out.println("Results can not be Calculated");
				}
				System.out.println("Contents of File ValueOfN.txt : "+LineN);
				System.out.println("Contents of File ValueOfR.txt : "+LineR);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}

class combClass extends PermutationsAndCombinations{
	public static void Combinations(){
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		try{
				System.out.println("Value Of N : ");
				String content = null;
				try{
					content=bufferedReader.readLine();
				}
				catch(IOException error){
					System.out.println("Input Error"+error.getMessage());
				}
				String path = "ValueOfN.txt";
				File file = new File(path);
				if(!file.exists()){
					file.createNewFile();
				}
				FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
				bufferedWriter.write(content);
				bufferedWriter.close();
			
			
				System.out.println("Value Of R : ");
				String contentR = null;
				try{
					contentR=bufferedReader.readLine();
				}
				catch(IOException error){
					System.out.println("Input Error"+error.getMessage());
				}
				
				String pathR = "ValueOfR.txt";
				File fileR = new File(pathR);
				if(!fileR.exists()){
					fileR.createNewFile();
				}
				
				FileWriter fileWriterR = new FileWriter(fileR.getAbsoluteFile());
				BufferedWriter bufferedWriterR = new BufferedWriter(fileWriterR);
				bufferedWriterR.write(contentR);
				bufferedWriterR.close();
		
			String lineN = content;
			String lineR = contentR;
			
			
				int LineN = Integer.parseInt(lineN);
				int LineR = Integer.parseInt(lineR);
				
				if(LineN>0 && LineR>0 && LineN>LineR){
					int Number = LineN - LineR;
					int value1=1, value2=1, value3 =1;
					
					for(int indexI=1; indexI<=LineN; indexI++){
						value1 = value1*indexI;
					}
					for(int indexI=1; indexI<=LineR; indexI++){
						value2 = value2*indexI;
					}
					for(int indexI=1; indexI<=Number; indexI++){
						value3 = value3*indexI;
					}
					
					int valueOfCombination = value1/(value3*value2);
					System.out.println("Result Of Combinations is : "+valueOfCombination);
					String result = Integer.toString(valueOfCombination);
					String pathValue = "ResultOfCombinations.txt";
					File ResultCom = new File(pathValue);
						if(!ResultCom.exists()){
							ResultCom.createNewFile();
						}
					FileWriter fileResultC = new FileWriter(ResultCom.getAbsoluteFile());
					BufferedWriter bufferedResult = new BufferedWriter(fileResultC);
					bufferedResult.write("Result Of Combinations is : "+result);
					bufferedResult.close();
				}
				else{
					System.out.println("Results can not be Calculated");
				}
				System.out.println("Contents of File ValueOfN.txt : "+LineN);
				System.out.println("Contents of File ValueOfR.txt : "+LineR);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}